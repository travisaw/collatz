#include <time.h>
#include <stdio.h>
#include "collatzTime.h"

time_t startTime;
time_t endTime;

int getRunTime()
{
    return endTime - startTime;
}

void startRun()
{
    startTime = time (NULL);
}

void endRun()
{
    endTime = time (NULL);
}

/*
int main ()
{
    time_t sec;
    sec = time (NULL);

    printf ("Number of hours since January 1, 1970 is %ld \n", sec/3600);

    time_t mytime;
    mytime = time(NULL);
    printf(ctime(&mytime));

    return 0;


}
*/