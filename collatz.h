#ifndef COLLATZ_H_
#define COLLATZ_H_

//-----------------------------------------
// GETTERS
long long int getLargeValue();
int getLargeCount();

//-----------------------------------------
// FUNCTIONS
long long int collatz(long long int n, int k);
void logLargeValues(long long int value, long long int iterations);

#endif // COLLATZ_H_