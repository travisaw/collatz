#include <stdio.h>
#include <stdlib.h>
#include "collatz.h"
#include "collatzTime.h"
#include "collatzConfig.h"

// Main body.
int main()
{

    initConfig();
    int increment = getPrintIncrement();


    // Declare the variable n.
    long long int n;
    // Set the counter to 0.
    int k = 0;
    // Message to user requesting input.
    printf("Enter an integer greater than 1: ");
    scanf("%lld", &n);
    // Clear the screen.
    // system("clear");
    // Display the starting integer.
    // printf("%d\n", n);
    // Run the recursion and
    // Display the number of iterations.
    startRun();
    long long int a;
    long long int c;
    for( a = 1; a <= n; a = a + 1 ){
        c = collatz(a,k);

        //printf("Conjecture for %d verified in %d iterations\n", a, c);
        if (a % increment == 0) {
            printf("Finished %lld iterations\n", a);
        }
        logLargeValues(a, c);
        //printf("Count %d\n", a);
    }
    printf("Largest number of iterations is %d on number %lld \n", getLargeCount(), getLargeValue());
    //End
    endRun();
    printf("Ran in %d seconds \n", getRunTime());
    return 0;
}
