#include <stdio.h>
#include <libconfig.h>
#include "collatzConfig.h"

config_t cfg, *cf;
int printIncrement;
const int printIncrementDefault = 100000;

int initConfig()
{
    cf = &cfg;
    if (!config_read_file(cf, "config.cfg")) {
        fprintf(stderr, "%s:%d - %s\n",
            config_error_file(cf),
            config_error_line(cf),
            config_error_text(cf));
        config_destroy(cf);
        //return(EXIT_FAILURE);
        return(0);
    }
}

int getPrintIncrement()
{
    if (config_lookup_int(cf, "PrintIncrement", &printIncrement))
    {
        printf("Using Increment %d\n", printIncrement);
        return(printIncrement);
    }
    else
    {
        printf("Parameter not found, using default %d\n", printIncrementDefault);
        return(printIncrementDefault);
    }
}
