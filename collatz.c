#include <stdio.h>
#include <stdlib.h>
#include "collatz.h"

// Global Variables
long long int gLargeValue = 0;
int gLargeCount = 0;

// Getters
long long int getLargeValue()
{
    return gLargeValue;
}

int getLargeCount()
{
    return gLargeCount;
}

// Define the recursive function.
//long long int collatz(long long int n, int k)
long long int collatz(long long int n, int k)
{
    // Exit case.
    if(n==1)
    {
        return k;
    }
    // Case where n is even.
    else if(n%2==0)
    {
        k = k+1;
        n = n/2;
        //printf("%d\n", n);
        collatz(n,k);
    }
    // Case where n is odd.
    else
    {
        k = k+1;
        n = 3*n+1;
        //printf("%d\n", n);
        collatz(n,k);
    }
}

void logLargeValues(long long int value, long long int iterations)
{
    if (iterations > gLargeCount) {
        gLargeCount = iterations;
        gLargeValue = value;
    }
}






