#ifndef COLLATZ_TIME_H_
#define COLLATZ_TIME_H_

//-----------------------------------------
// FUNCTIONS

int getRunTime();
void startRun();
void endRun();

#endif // COLLATZ_TIME_H_